# TwoDrive
![Alt text](screenshot.png "Optional title")

👷 **================= Work in progress =================** 👷

TwoDrive (Name likely to change) is a web-based file browser inspired by Google Drive and OneDrive, created with React and Express. This repository houses designs and some early code.

## Install
``` cd twodriveapp-frontend ```

``` npm i ```

## Run
``` npm start```


Browse to ```http://localhost:3001/my-drive``` to see the main interface.