import React from 'react';
import ReactDom from 'react-dom';
import './index.css';
import App from './components/App/App.jsx';

ReactDom.render(<App />, document.getElementById('root'));
