import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Login from '../Login/Login';
import useToken from './useToken';
import MyDrive from '../MyDrive/MyDrive';

function App() {
    const { token, setToken } = useToken();
    console.log(token);

    if (!token) {
    //    return <Login setToken={setToken} />  will re-enable this when I fix it
    }
    return (
        <div className="wrapper">
            <BrowserRouter>
                <Switch>
                    <Route path="/my-drive">
                        <MyDrive />
                    </Route>
                </Switch>
            </BrowserRouter>
        </div>
    );
}
export default App;
