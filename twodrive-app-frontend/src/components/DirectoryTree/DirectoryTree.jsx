import { ReactComponent as FolderSmall } from './folder-small.svg';

function hideDirectories() {
    
}

export default function DirectoryTree() {
    return (
        <div className="DirectoryTree">
            <a href=""  onClick={hideDirectories} className="DirectoryTree__item DirectoryTree__item__root"><div className="expanded-arrow"></div>My Files</a>
            <div className="DirectoryTree__group">
                <a href="/" className="DirectoryTree__item"><div className="collapsed-arrow"></div><FolderSmall />Documents</a>
                <a href="/" className="DirectoryTree__item"><div className="collapsed-arrow"></div><FolderSmall />Documents</a>
                <a href="/" className="DirectoryTree__item"><div className="collapsed-arrow"></div><FolderSmall />Documents</a>
            </div>
        </div>
    );
}
