import React from 'react';
import DirectoryTree from '../DirectoryTree/DirectoryTree';
import FileBrowser from '../FileBrowser/FileBrowser';
import MainNav from '../MainNav/MainNav';

export default function MyDrive() {
    return (
        <div>
            <MainNav />
            <div className="MainWindow">
                <DirectoryTree />
                <FileBrowser />
            </div>
        </div>
    );
}
