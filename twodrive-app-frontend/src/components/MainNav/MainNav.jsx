import { ReactComponent as Logo } from './logo.svg';
import ProfilePicture from './profilepic.png'

export default function MainNav() {
    return (
        <div className="MainNav">
            <a href="/" className="MainNav__link"><Logo /></a>
            <div className="MainNav__profile">
                <a href="/" className="MainNav__profile__picture"><img src={ProfilePicture} alt="" /></a>
                <p>Signed in as Jonathan</p>
            </div>
        </div>
    );
}
