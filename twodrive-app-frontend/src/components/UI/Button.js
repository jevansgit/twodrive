import React from 'react';

export default class Button extends React.Component {
    handleClick = () => { };
    render() {
        return (
            <button className="button" > {this.props.title}</button>
        );
    }
}
