import Button from '../UI/Button.js';
import Folder from './Folder.js';

export default function FileBrowser() {
    return (
        <div className="FileBrowser">
            <FileBrowserNavigation />
            <FileBrowserMainPanel />
        </div>
    );
}

function FileBrowserNavigation() {
    return (
        <div className="FileBrowser__Navigation">
            <p className="FileBrowser__Navigation__path">MyFiles &gt;</p>
            <Button title="New..." />
        </div>
    );
}

function FileBrowserMainPanel() {
    return (
        <div className="FileBrowser__MainPanel">
            <Folder title="Documents" />
            <Folder title="Documents" />
            <Folder title="Documents" />
        </div>
    );
}
