import React from 'react';
import { ReactComponent as FolderLarge } from './folder-large.svg';

/*
 * The is a folder component that will keep track of if it has
 * been clicked, or highlighted with a selection rectangle. This 
 * will work similar to how your operating system's file browser
 * may work.
 */
export default class Folder extends React.Component {
    constructor(props) {
        super(props);
        this.state = { 
            clicked: null,
            highlighted: null,
        };
    }
    render() {
        let clickedStyle = {
            background: 'red'
        };
        return (
            <div className="folder" onClick={(this.state.clicked = true)}>
                <FolderLarge />
                <p>{this.props.title}</p>
            </div>
        );
    }
}
