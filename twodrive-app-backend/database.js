var sqlite3 = require('sqlite3').verbose();
var md5 = require('md5');

const dbSource = "db.sqlite"

let db = new sqlite3.Database(dbSource, (err) => {
    if (err) {
        console.log(err.message);
        throw err
    } else {
        console.log('✔ Database (SQLite) connected successfully');
        db.run(`CREATE TABLE user (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            username text UNIQUE,
            password text,
            token text,
            CONSTRAINT username_unique UNIQUE (username)
        )`,
        (err) => {
            if (err) {
                // Table created already
            } else {
                var insert = 'INSERT INTO user (username, password) VALUES (?, ?)';
                db.run(insert, ["Jon",md5("123456")]);
                db.run(insert, ["Bob",md5("123456")]);
                db.run(insert, ["Andrew",md5("123456")]);
            }
        });
    }
});

module.exports = db;
