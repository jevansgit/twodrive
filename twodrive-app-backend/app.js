const cors = require('cors');
var express = require("express");
var app = express();
var db = require("./database.js");
require('dotenv').config();
const jwt = require('jsonwebtoken');
const md5 = require("md5");

app.use(cors());
app.use(express.json());



var port = 4001;

app.listen(port, () =>  {
    console.log("✔ Server running on port", port)
});

app.get("/", (req, res, next) => {
    res.json({"message": "Ok"})
});


app.post("/api/login", async (req, res, next) => {
    const {username, password} = req.body;

    // Authenticate user SQL
    var sql = "SELECT  id, username, password FROM user WHERE username = ?";
    var params = [username];
    db.get(sql, params, (err, userFromDb) => {
        if (err) {
            res.status(400).json({"error": err.message});
            return;
        }

        // Authorize
        // Check password
        console.log(password)
        try {
            if (!userFromDb.password == md5(password))
                return;
        } catch(err) {
            console.log("user/pass does not match");
        }

        // Send token to browser
        const user = { name: username}
        const token = jwt.sign(user, process.env.TOKEN_KEY, {expiresIn: "2h"});
        res.json({ token: token })

        // Update token in database
        sql = "UPDATE user SET token = ? WHERE id = ?";
        params = [token, userFromDb.id]
        db.run(sql, params, function(err){
            if (err)
                console.log(err.message);
        });
    });
})



app.get("/api/users", (req, res, next) => {
    var sql = "select * from user";
    var params = [];
    db.all(sql, params, (err, rows) => {
        if (err) {
            res.status(400).json({"error": err.message});
            return;
        }
        res.json ({
            "message": "success",
            "data:": rows
        })
    })
})

app.get("/api/user/:id", (req, res, next) => {
    var sql = "select * from user where id = ?"
    var params = [req.params.id];
    db.get(sql, params, (err, row) => {
        if (err) {
            res.status(400).json({"error": err.message});
            return;
        }
        res.json({
            "message":"success",
            "data":row,
        })
    })
})

// More api endpoints

app.use(function(req, res) {
    res.status(404);
})